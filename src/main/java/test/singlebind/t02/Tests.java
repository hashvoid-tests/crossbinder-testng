/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package test.singlebind.t02;

import org.testng.Assert;
import org.testng.annotations.Test;

import rd.testng.XbinderTest1;

/**
 * If a singleton implements multiple interfaces, none of which are annotated with either @Bindable
 * or @NonBindable, then the singleton can be referenced through all interfaces. Furthermore, the
 * same instance of the singleton is referenced at all times.
 *
 * @author randondiesel
 *
 */

public class Tests extends XbinderTest1 {

	@Test
	public void execute() throws Throwable {
		rethrowSetupError();

		Ezfb1 obj1 = crossbinder().locator().get(Ezfb1.class);
		Ezfb2 obj2 = crossbinder().locator().get(Ezfb2.class);

		Assert.assertNotNull(obj1);
		Assert.assertNotNull(obj2);
		Assert.assertSame(obj1, obj2);
	}
}
