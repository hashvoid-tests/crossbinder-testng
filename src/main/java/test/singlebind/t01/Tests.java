/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package test.singlebind.t01;

import org.testng.Assert;
import org.testng.annotations.Test;

import rd.testng.Trace;
import rd.testng.XbinderTest1;

/**
 * If a singleton entity is not annotated with @Singleton, it is not bound with Crossbinder.
 *
 * @author randondiesel
 *
 */

public class Tests extends XbinderTest1 {

	@Test
	public void execute() throws Throwable {
		rethrowSetupError();

		Object value = Trace.getInst().get(DoleNotSing.class.getName());
		Dole1 obj1 = crossbinder().locator().get(Dole1.class);
		Dole2 obj2 = crossbinder().locator().get(Dole2.class);
		Assert.assertNull(value);
		Assert.assertNull(obj1);
		Assert.assertNull(obj2);
	}
}
