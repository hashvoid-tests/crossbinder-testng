/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package test.protobind.t08;

import org.testng.Assert;
import org.testng.annotations.Test;

import rd.testng.XbinderTest1;

/**
 * If a prototype implements multiple interfaces, derives from a base class, that in turn implements
 * multiple interfaces, and some of the interfaces in the hierarchy are bindable, then the prototype
 * can be referenced using only the bindable interfaces. Furthermore, a new instance of the
 * prototype is obtained for each reference made.
 * <p>
 *
 * @author randondiesel
 *
 */

public class Tests extends XbinderTest1 {

	@Test
	public void execute() {
		Jzpbh1 obj1 = crossbinder().locator().get(Jzpbh1.class);
		Jzpbh2 obj2 = crossbinder().locator().get(Jzpbh2.class);
		Jzpbh3 obj3 = crossbinder().locator().get(Jzpbh3.class);
		Jzpbh4 obj4 = crossbinder().locator().get(Jzpbh4.class);

		Assert.assertNull(obj1);
		Assert.assertNotNull(obj2);
		Assert.assertNull(obj3);
		Assert.assertNotNull(obj4);
		Assert.assertNotSame(obj2, obj4);
	}
}
