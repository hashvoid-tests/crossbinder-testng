/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package test.protobind.t03;

import org.testng.Assert;
import org.testng.annotations.Test;

import rd.testng.XbinderTest1;

/**
 * If a prototype implements multiple interfaces, and some of the interfaces are annotated with
 * @Bindable, then the prototype can be referenced only through the bindable interfaces.
 * Furthermore, a new instance of the prototype is referenced each time through any of the bindable
 * interfaces.
 * <p>
 *
 * @author randondiesel
 *
 */

public class Tests extends XbinderTest1 {

	@Test
	public void execute() {
		Qhfpo obj1 = crossbinder().locator().get(Qhfpo.class);
		QhfpoBind1 obj2 = crossbinder().locator().get(QhfpoBind1.class);
		QhfpoBind2 obj3 = crossbinder().locator().get(QhfpoBind2.class);

		Assert.assertNull(obj1);
		Assert.assertNotNull(obj2);
		Assert.assertNotNull(obj3);
		Assert.assertNotSame(obj2, obj3);
	}
}
