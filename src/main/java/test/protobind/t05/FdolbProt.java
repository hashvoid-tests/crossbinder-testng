/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package test.protobind.t05;

import rd.crossbinder.hod.Prototype;
import rd.testng.Trace;

/**
 * @author randondiesel
 *
 */

@Prototype
public class FdolbProt implements FdolbNoBind1, FdolbNoBind2 {

	public FdolbProt() {
		Trace.getInst().set(FdolbProt.class.getName(), "created");
	}
}
