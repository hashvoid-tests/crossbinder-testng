/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.hashvoid.testng.crossbinder.protoinit;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.hashvoid.testng.crossbinder.protoinit.fix01.LazyProtoA;
import com.hashvoid.testng.crossbinder.protoinit.fix01.ProtoA;
import com.hashvoid.testng.crossbinder.protoinit.fix01.SingleFace;
import com.hashvoid.testng.crossbinder.protoinit.fix02.FaceB1;
import com.hashvoid.testng.crossbinder.protoinit.fix02.FaceB2;
import com.hashvoid.testng.crossbinder.protoinit.fix02.InitMthdProtoB;
import com.hashvoid.testng.crossbinder.protoinit.fix02.InitProtoB;

import rd.crossbinder.hod.Crossbinder;
import rd.testng.Trace;

/**
 * @author randondiesel
 *
 */

public class TestScenarios {

/**
 * A prototype that is not defined to be lazy-loading is instantiated at the same time when the
 * entities where they are injected are created.
 *
 * @param tctxt
 */

	@Test
	public void execute01(ITestContext tctxt) {
		Crossbinder xb = null; //XbinderFixture.getInst(tctxt);
		Object tval1 = Trace.getInst().get(ProtoA.class.getName());

		SingleFace obj1  = xb.locator().get(SingleFace.class);
		String id = obj1.getProtoId1();

		Object tval2 = Trace.getInst().get(ProtoA.class.getName());

		Assert.assertNull(tval1);
		Assert.assertNotNull(tval2);
	}

/**
 * A Prototype that is defined to be lazy-loading is instantiated only at the time of first method
 * invocation on the corresponding referenced/injected entity.
 *
 * @param tctxt
 */

	@Test
	public void execute02(ITestContext tctxt) {
		Crossbinder xb = null; //XbinderFixture.getInst(tctxt);
		Object tval1 = Trace.getInst().get(LazyProtoA.class.getName());
		SingleFace obj1  = xb.locator().get(SingleFace.class);
		String id = obj1.getProtoId2();
		Object tval2 = Trace.getInst().get(LazyProtoA.class.getName());

		Assert.assertNull(tval1);
		Assert.assertNotNull(tval2);
		Assert.assertSame(tval2, id);
	}

/**
 * A prototype that implements Initializable, has the corresponding initialize() method invoked at
 * the time of startup.
 *
 * @param tctxt
 */

	@Test(description = "A prototype that implements Initializable, has the corresponding "
			+ "initialize() method invoked at the time of startup.")
	public void execute03(ITestContext tctxt) {
		Crossbinder xb = null; //XbinderFixture.getInst(tctxt);
		FaceB1 obj1  = xb.locator().get(FaceB1.class);

		Object tval = Trace.getInst().get(InitProtoB.class.getName());

		Assert.assertNotNull(tval);
		Assert.assertEquals(tval, "initialized");
	}

/**
 * A method in a prototype with zero parameters, void return type and annotated with @Initializable
 * is invoked at the time of startup
 *
 * @param tctxt
 */

	@Test
	public void execute04(ITestContext tctxt) {
		Crossbinder xb = null; //XbinderFixture.getInst(tctxt);
		FaceB2 obj1  = xb.locator().get(FaceB2.class);

		Object tval = Trace.getInst().get(InitMthdProtoB.class.getName());

		Assert.assertNotNull(tval);
		Assert.assertEquals(tval, "initialized3");
	}

}
