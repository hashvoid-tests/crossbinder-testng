/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.hashvoid.testng.crossbinder.singleinit;

import java.util.Set;
import java.util.logging.Logger;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.hashvoid.testng.crossbinder.singleinit.fix01.FaceA1;
import com.hashvoid.testng.crossbinder.singleinit.fix01.FaceA2;
import com.hashvoid.testng.crossbinder.singleinit.fix01.LazySingleA;
import com.hashvoid.testng.crossbinder.singleinit.fix01.SingleA;
import com.hashvoid.testng.crossbinder.singleinit.fix02.InitMthdSingleB;
import com.hashvoid.testng.crossbinder.singleinit.fix02.InitSingleB;
import com.hashvoid.testng.crossbinder.singleinit.fix03.FaceC1;
import com.hashvoid.testng.crossbinder.singleinit.fix03.FaceC2;
import com.hashvoid.testng.crossbinder.singleinit.fix03.InitLazySingleC;
import com.hashvoid.testng.crossbinder.singleinit.fix03.InitMthdLazySingleC;
import com.hashvoid.testng.crossbinder.singleinit.fix04.SlowFaceD;
import com.hashvoid.testng.crossbinder.singleinit.fix05.InitMthdShadowSingleE;
import com.hashvoid.testng.crossbinder.singleinit.fix05.InitShadowSingleE;
import com.hashvoid.testng.crossbinder.singleinit.fix05.LazyShadowSingleE;

import rd.crossbinder.hod.Crossbinder;
import rd.testng.Trace;

/**
 * @author randondiesel
 *
 */

public class TestScenarios {

	private static final Logger LOGGER = Logger.getLogger(TestScenarios.class.getName());
/**
 * A lazy-loading singleton is instantiated the first time a method on it is invoked.
 *
 * @param tctxt
 */

	@Test(description="A lazy-loading singleton is instantiated the first time a method on it "
			+ "is invoked.")
	public void execute01(ITestContext tctxt) {
		Crossbinder xb = null; //XbinderFixture.getInst(tctxt);
		Object tval1 = Trace.getInst().get(LazySingleA.class.getName());

		FaceA1 obj = xb.locator().get(FaceA1.class);
		Object tval2 = Trace.getInst().get(LazySingleA.class.getName());

		Assert.assertNull(tval1);
		Assert.assertNotNull(obj);
		Assert.assertNull(tval2);

		obj.callme();

		Object tval3 = Trace.getInst().get(LazySingleA.class.getName());
		Assert.assertNotNull(tval3);
	}

/**
 * A singleton that is not marked as lazy-loading is instantiated at the time of Crossbinder start.
 *
 * @param tctxt
 */

	@Test(description="A singleton that is not marked as lazy-loading is instantiated at the time "
			+ "of Crossbinder start.")
	public void execute02(ITestContext tctxt) {
		Crossbinder xb = null; //XbinderFixture.getInst(tctxt);
		Object tval = Trace.getInst().get(SingleA.class.getName());

		FaceA2 obj = xb.locator().get(FaceA2.class);

		Assert.assertNotNull(tval);
		Assert.assertNotNull(obj);
	}

/**
 * A singleton that implements Initializable, has the corresponding initialize() method invoked at
 * the time of startup.
 *
 * @param tctxt
 */

	@Test(description = "A singleton that implements Initializable, has the corresponding "
			+ "initialize() method invoked at the time of startup.")
	public void execute03(ITestContext tctxt) {
		Object tval = Trace.getInst().get(InitSingleB.class.getName());

		Assert.assertNotNull(tval);
		Assert.assertEquals(tval, "initialized");
	}

/**
 * A method in a singleton with zero parameters, void return type and annotated with @Initializable
 * is invoked at the time of startup
 *
 * @param tctxt
 */

	@Test
	public void execute04(ITestContext tctxt) {
		Object tval = Trace.getInst().get(InitMthdSingleB.class.getName());

		Assert.assertNotNull(tval);
		Assert.assertEquals(tval, "initialized3");
	}

/**
 * A lazy-loading singleton that implements Initializable has the corresponding initialize() method
 * executed the first time a method on it is invoked.
 *
 * @param tctxt
 */

	@Test(description="A lazy-loading singleton that implements Initializable is initialized the"
			+ "first time a method on it is invoked.")
	public void execute05(ITestContext tctxt) {
		Crossbinder xb = null; //XbinderFixture.getInst(tctxt);
		Object tval1 = Trace.getInst().get(InitLazySingleC.class.getName());

		FaceC1 obj = xb.locator().get(FaceC1.class);
		Object tval2 = Trace.getInst().get(InitLazySingleC.class.getName());

		Assert.assertNull(tval1);
		Assert.assertNotNull(obj);
		Assert.assertNull(tval2);

		obj.callme();

		Object tval3 = Trace.getInst().get(InitLazySingleC.class.getName());
		Assert.assertNotNull(tval3);
	}

/**
 * A lazy-loading singleton that does not implement Initializable, has the corresponding method
 * (with the correct syntax) annotated with @Initialize, executed the first time a method on it is
 * invoked.
 *
 * @param tctxt
 */

	@Test(description = "A lazy-loading singleton that does not implement Initializable, has the "
			+ "corresponding method (with the correct syntax) annotated with @Initialize, executed "
			+ "the first time a method on it is invoked.")
	public void execute06(ITestContext tctxt) {
		Crossbinder xb = null; //XbinderFixture.getInst(tctxt);
		Object tval1 = Trace.getInst().get(InitMthdLazySingleC.class.getName());

		FaceC2 obj = xb.locator().get(FaceC2.class);
		Object tval2 = Trace.getInst().get(InitMthdLazySingleC.class.getName());

		Assert.assertNull(tval1);
		Assert.assertNotNull(obj);
		Assert.assertNull(tval2);

		obj.callme();

		Object tval3 = Trace.getInst().get(InitMthdLazySingleC.class.getName());
		Assert.assertNotNull(tval3);
		Assert.assertEquals(tval3, "initialized3");
	}

/**
 * Lazy-loading singletons, that can potentially be initialized simultaneously from multiple threads
 * still has exactly one copy created and maintained with crossbinder.
 *
 * @param tctxt
 */

	@Test(threadPoolSize=5, invocationCount=15, description =
		"Lazy-loading singletons, that can potentially be initialized simultaneously from multiple "
		+ "threads still has exactly one copy created and maintained with crossbinder.")
	public void execute07(ITestContext tctxt) {
		Crossbinder xb = null; //XbinderFixture.getInst(tctxt);
		SlowFaceD obj = xb.locator().get(SlowFaceD.class);

		Set<String> idset = obj.getIdSet();
		LOGGER.fine(String.format("id set contains: %s", idset.toString()));
		Assert.assertEquals(idset.size(), 1);
	}

/**
 * Lazy loading directive for a shadow singleton has no effect. The singleton is still loaded at the
 * time of crossbinder start.
 *
 */

	@Test(description = "Lazy loading directive for a shadow singleton has no effect. The "
			+ "singleton is still loaded at the time of crossbinder start.")
	public void execute08() {
		Object tval = Trace.getInst().get(LazyShadowSingleE.class.getName());
		Assert.assertNotNull(tval);
		Assert.assertEquals(tval, "created");
	}

/**
 * The rules of initialization also works for shadow singletons.
 *
 */

	@Test(description = "The rules of initialization also works for shadow singletons.")
	public void execute09() {
		Object tval1 = Trace.getInst().get(InitShadowSingleE.class.getName());
		Assert.assertNotNull(tval1);
		Assert.assertEquals(tval1, "initialized");

		Object tval2 = Trace.getInst().get(InitMthdShadowSingleE.class.getName());
		Assert.assertNotNull(tval2);
		Assert.assertEquals(tval2, "initialized");
	}
}
