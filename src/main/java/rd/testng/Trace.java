/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package rd.testng;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author randondiesel
 *
 */

public class Trace {

	private static final Logger LOGGER = Logger.getLogger(Trace.class.getName());

	private static final Trace trace = new Trace();

	public static final Trace getInst() {
		return trace;
	}

	////////////////////////////////////////////////////////////////////////////
	// Non-static fields and methods

	private List<String> entries;
	private Map<String, Object> attribs;

	private Trace() {
		entries = new LinkedList<>();
		attribs = new HashMap<>();
	}

	public void reset() {
		entries.clear();
		attribs.clear();
	}

	public void write(String message) {
		entries.add(message);
		LOGGER.fine(message);
	}

	public int count() {
		return entries.size();
	}

	public boolean contains(String message) {
		return entries.contains(message);
	}

	public String at(int pos) {
		if(pos < 0 || pos >= entries.size()) {
			return null;
		}
		return entries.get(pos);
	}

	public void set(String name, Object value) {
		attribs.put(name, value);
	}

	public Object get(String name) {
		return attribs.get(name);
	}
}
